Näissä ei ole committeja, koska loin kurssin alussa aina uuden projektin jokaiselle tehtävälle, 
mutta en osannut laittaa monta eri projektia samaan repositoryyn.

Tästä syystä jouduin lopuksi luomaan uuden projektin, johon toin kaikki muut projektit ja sain 
ohjelman jotenkin toimimaan.