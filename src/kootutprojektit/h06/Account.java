/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kootutprojektit.h06;

import java.util.Scanner;

/**
 * @author n1602
 * Teijo Pekkola
 * IT-kesäleiri 2018
 * 31.5.2018
 */

abstract public class Account {
    
    protected String tilinumero;
    protected int saldo;
    Scanner scan;

    
    public Account() {
        String tiliNro;
        String rahaaStr;
        scan = new Scanner(System.in);
        
        System.out.print("Syötä tilinumero: ");
        tiliNro = scan.nextLine();
        System.out.print("Syötä rahamäärä: ");
        rahaaStr = scan.nextLine();
        
        int rahaa = Integer.parseInt(rahaaStr);
        
        tilinumero = tiliNro;
        saldo = rahaa;
    }

    public String getTilinumero() {
        return tilinumero;
    }
    public int getSaldo() {
        return saldo;
    }
    
    /**
     * lisätään rahaa tilille
     * @param amount 
     */
    public void talletaRahaa(int amount) {
        saldo += amount;
    }
    
    /**
     * Nostetaan rahaa tililtä. saldo ei saa mennä miinukselle
     * palautetaan true, jos nosto onnistui.
     * @param amount
     * @return success
     */
    public boolean nostaRahaa(int amount) {
        boolean success = false;
        if (saldo >= amount) {
            saldo -= amount;
            success = true;
        }
        return success;
    }
    
    public void tulostaTiedot() {
        System.out.println("Tilinumero: " + getTilinumero() + " Tilillä rahaa: " + getSaldo() );
    }
    
}


class TavallinenTili extends Account {
    
}

/**
 * Kuin tavallinen tili, mutta saldo voi mennä miinukselle luottorajan verran.
*/
class LuottoTili extends Account {
    
    protected int luottoraja;
    
    public LuottoTili() {
        System.out.print("Syötä luottoraja: ");
        String luottorajaStr = scan.nextLine();
        luottoraja = Integer.parseInt(luottorajaStr);
    }
    
    public int getLuottoraja() {
        return luottoraja;
    }
    public void setLuottoraja(int uusiRaja) {
        luottoraja = uusiRaja;
    }
    
    /**
     * saldo voi mennä miinukselle luottorajan verran
     * palautetaan true jos nosto onniastui.
     * @param amount
     * @return success
     */
    public boolean nostaRahaa(int amount) {
        boolean success = false;
        if (saldo + luottoraja >= amount) {
            saldo -= amount;
            success = true;
        }
        return success;
    }
    
    /**
     * Tulostetaan myös luottoraja
     */
    public void tulostaTiedot() {
        System.out.println("Tilinumero: " + getTilinumero() + " Tilillä rahaa: " + getSaldo() + " Luottoraja: " + getLuottoraja());
    }

    
}