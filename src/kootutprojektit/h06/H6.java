/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kootutprojektit.h06;

import java.util.Scanner;

/**
 * @author n1602
 * Teijo Pekkola
 * IT-kesäleiri 2018
 * 30.5.2018
 */
public class H6 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        boolean run = true;
        int input;
        
        Bank bank = new Bank();
        
        while (run) {
            H6.bankUI();
            input = H6.getInput();
            
            switch(input) {
                case 1:
                    bank.lisaaTavallinenTili();
                    
                    break;
                case 2:
                    bank.lisaaLuottoTili();
                    break;
                case 3:
                    bank.talletaRahaa();
                    break;
                case 4:
                    bank.nostaRahaa();
                    break;
                case 5:
                    bank.poistaTili();
                    break;
                case 6:
                    bank.tulostaTili();
                    break;
                case 7:
                    bank.tulostaKaikkiTilit();
                    
                    break;
                case 0:
                    run = false;
                    break;
                default:
                    System.out.println("Valinta ei kelpaa.");
                    break;
                    
            
            }
            
        }
    }
    /**
     * Tulostetaan valikko
     */
    public static void bankUI() {
        System.out.println("");
        System.out.println("*** PANKKIJÄRJESTELMÄ ***");
        System.out.println("1) Lisää tavallinen tili");
        System.out.println("2) Lisää luotollinen tili");
        System.out.println("3) Tallenna tilille rahaa");
        System.out.println("4) Nosta tililtä");
        System.out.println("5) Poista tili");
        System.out.println("6) Tulosta tili");
        System.out.println("7) Tulosta kaikki tilit");
        System.out.println("0) Lopeta");
    }
    
    /**
     * Pyydetään valinta, ja palautetaan valinta integerinä. jos syötettä ei 
     * voitu muuttaa integeriksi, palautetaan -1
     * @return inputInt
     */
    public static int getInput() {
        String inputStr;
        int inputInt;
        
        Scanner scan = new Scanner(System.in);
        
        System.out.print("Valintasi: ");
        inputStr = scan.nextLine();
        
        // Muunnetaan str -> int
        try {
            inputInt = Integer.parseInt(inputStr);
        } catch (NumberFormatException e) {
            inputInt = -1;
        }
            
        return inputInt;
    }
    
}
