/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kootutprojektit.h06;

import java.util.ArrayList;
import java.util.Scanner;

/**
 * @author n1602
 * Teijo Pekkola
 * IT-kesäleiri 2018
 * 30.5.2018
 */
public class Bank {
    
    ArrayList<Account> tilit;
    Scanner scan;
    
    public Bank() {
        tilit = new ArrayList<>();
        scan = new Scanner(System.in);
    }
    
    /**
     * Lisätään tavallinen tili
     */
    public void lisaaTavallinenTili() {
        TavallinenTili lisattavaTili = new TavallinenTili();
        tilit.add(lisattavaTili);
        System.out.println("Tili luotu.");
    }
    /**
     * Lisätään luottorajallinen tili 
     * (tämä ja ylempi metodi voitaisiin ehkä yhdistää jotenkin)
     */
    public void lisaaLuottoTili() {
        LuottoTili lisattavaTili = new LuottoTili();
        tilit.add(lisattavaTili);
        System.out.println("Tili luotu.");
    }
    
    /**
     * Haetaan tili tilinumeron perusteella ja talletetaan rahaa jos tili löytyi
     */
    public void talletaRahaa() {
        String tiliNro;
        String rahaaStr;
        int rahaaInt;
        
        System.out.print("Syötä tilinumero: ");
        tiliNro = scan.nextLine();
        System.out.print("Syötä rahamäärä: ");
        rahaaStr = scan.nextLine();
        
        rahaaInt = Integer.parseInt(rahaaStr);
        
        Account loytynytTili = etsiTili(tiliNro);
                
        if (loytynytTili != null) {
            loytynytTili.talletaRahaa(rahaaInt);         
        }
        
    }
    
    /**
     * Haetaan tili tilinumeron perusteella ja nostetaan rahaa jos tili löytyi
     */
    public void nostaRahaa() {
        String tiliNro;
        String rahaaStr;
        int rahaaInt;
        
        System.out.print("Syötä tilinumero: ");
        tiliNro = scan.nextLine();
        System.out.print("Syötä rahamäärä: ");
        rahaaStr = scan.nextLine();
        
        rahaaInt = Integer.parseInt(rahaaStr);
        
        Account loytynytTili = etsiTili(tiliNro);
                
        if (loytynytTili != null) {
            if ( loytynytTili.nostaRahaa(rahaaInt) ) {
                // Tähän suoritettava koodi, jos nosto onnistui
            } else {
                System.out.println("Tilillä ei ollut tarpeeksi rahaa.");
            }
        }
        
    }
    
    
    /**
     * Haetaan tili tilinumeron perusteella ja tulostetaan tiedot jos tili löytyi
     */
    public void tulostaTili() {
        String tutkittavaTilinumero;
        
        System.out.print("Syötä tulostettava tilinumero: ");
        tutkittavaTilinumero = scan.nextLine();
        
        Account loytynytTili = etsiTili(tutkittavaTilinumero);
        
        if (loytynytTili != null) {
            loytynytTili.tulostaTiedot();
        }

    }
    
    /**
     * Tulostetaan kaikkien tilien tiedot
     */
    public void tulostaKaikkiTilit() {
        int len = tilit.size();
        Account loytynytTili = null;
        
        System.out.println("Kaikki tilit:");
        
        for (int i=0 ; i<len ; i++) {
            loytynytTili = tilit.get(i);
            loytynytTili.tulostaTiedot();
        }

    }
    
    /**
     * Etsitään tili tilinumeron perusteella ja poistetaan, jos tili löytyi.
     */
    public void poistaTili() {
        String tutkittavaTilinumero;
        Account poistettavaTili;
        
        System.out.print("Syötä poistettava tilinumero: ");
        tutkittavaTilinumero = scan.nextLine();
        
        
        poistettavaTili = etsiTili(tutkittavaTilinumero);
        
        if (tilit.remove(poistettavaTili)) {
            System.out.println("Tili poistettu.");
        }
    }
    
    /**
     * Käydään kaikki tilit läpi ja esitään vastaako minkään tilin tilinumero
     * etsittyä numeroa. jos löytyi palautetaan tili-olio, muuten palautetaan null
     * @param tutkittavaTilinumero
     * @return loytynytTili
     */
    public Account etsiTili(String tutkittavaTilinumero) {
        int len = tilit.size();
        Account loytynytTili = null;
        
        for (int i=0 ; i<len ; i++) {
            if ( tutkittavaTilinumero.equals(tilit.get(i).getTilinumero()) ) {
                loytynytTili = tilit.get(i);
            }
        
        }
        if (loytynytTili == null) {
            System.out.println("Tiliä ei löytynyt.");
        }
        
        return loytynytTili;
    }
    
}

