/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kootutprojektit.h07;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;

/**
 *
 * @author n1602
 */
public class FXMLDocument_t5Controller implements Initializable {
    
    @FXML
    private Button saveButton;
    @FXML
    private Button loadButton;
    @FXML
    private TextField tiedostonimiField;
    @FXML
    private TextArea inputText;

    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void saveFile(ActionEvent event) {
        
        String fileName = tiedostonimiField.getText() + ".txt";
        
        writeFile(fileName);
    }
     
    @FXML
    private void loadFile(ActionEvent event) {
            
        String fileName = tiedostonimiField.getText() + ".txt";
        
        readFile(fileName);
    }
    
    private void writeFile(String tiedostonimi) {
        try {
            BufferedWriter out;
            out = new BufferedWriter(new FileWriter(tiedostonimi));
            
            out.write(inputText.getText());
            
            
            out.close();
            
        } catch (FileNotFoundException ex) {
            System.out.println("File not found.");
        } catch (IOException ex) {
        }
    }
    
    private void readFile(String tiedostonimi) {
        try {
            BufferedReader in;
            String inputLine;
            
            in = new BufferedReader(new FileReader(tiedostonimi));
            
            
            inputText.clear();
            while ((inputLine = in.readLine()) != null) {
                
                inputText.setText(inputText.getText() + inputLine);
                
                //out.write(inputLine + System.lineSeparator());

            }
            in.close();
            
        } catch (FileNotFoundException ex) {
            inputText.setText("Tiedostoa ei löydy.");
        } catch (IOException ex) {
        }
        
    }
    
}
