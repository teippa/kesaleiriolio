/**
 * Teijo Pekkola
 * IT-Kesäleiri 2018
 * 31.5.2018
 * 
 * TÄSSÄ ON NYT TEHTÄVÄT 3 JA 4 SAMASSA
 */
package kootutprojektit.h07;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

/**
 * @author n1602
 */
public class H7 extends Application {
    
    @Override
    public void start(Stage stage) throws Exception {
        Parent root1 = FXMLLoader.load(getClass().getResource("FXMLDocument_t4.fxml"));
        
        Scene scene1 = new Scene(root1);
        
        stage.setScene(scene1);
        stage.show();
        stage.toFront();
        
        
        Stage stage2 = new Stage();
        Parent root2 = FXMLLoader.load(getClass().getResource("FXMLDocument_t5.fxml"));
        
        Scene scene2 = new Scene(root2);
        
        stage2.setScene(scene2);
        stage2.show();
        stage2.toFront();
        
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }
    
}
