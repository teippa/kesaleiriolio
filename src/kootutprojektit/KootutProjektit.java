/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kootutprojektit;

import java.util.Scanner;
import javafx.application.Application;
import javafx.stage.Stage;
import kootutprojektit.h05.H5;
import kootutprojektit.h06.H6;
import kootutprojektit.h07.H7;
import kootutprojektit.h08.H8;
import kootutprojektit.h09.H9;
import kootutprojektit.h10.H10;
import kootutprojektit.h11.H11;


/**
 * 
 * @author n1602
 */
public class KootutProjektit {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        System.out.print("Syötä tehtävänumero (5-11)\n -");
        
        int project;
        project = getInput();
        
        System.out.println("\n----------------------------------------\n");
        
        switch (project) {
            case 5:
                H5.main(args);
                break;
            case 6:
                H6.main(args);
                break;
            case 7:
                H7.main(args);
                break;
            case 8:
                H8.main(args);
                break;
            case 9:
                H9.main(args);
                break;
            case 10:
                H10.main(args);
                break;
            case 11:
                H11.main(args);
                break;
            default:
                System.out.println("Numerolla ei löytynyt tehtävää");
                break;
                
        }
        
        
        
        
        //launch(args);
    }
 
    
    
    
    /**
     * Pyydetään valinta, ja palautetaan valinta integerinä. jos syötettä ei 
     * voitu muuttaa integeriksi, palautetaan -1
     * @return inputInt
     */
    public static int getInput() {
        String inputStr;
        int inputInt;
        
        Scanner scan = new Scanner(System.in);
        
        inputStr = scan.nextLine();
        
        // Muunnetaan str -> int
        try {
            inputInt = Integer.parseInt(inputStr);
        } catch (NumberFormatException e) {
            inputInt = -1;
        }
            
        return inputInt;
    }
}
