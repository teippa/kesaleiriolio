/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kootutprojektit.h09;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import javafx.scene.text.Font;

/**
 *
 * @author n1602
 */
public class FXMLDocumentController implements Initializable {
    
    Finnkino kino;
    
    @FXML
    private TextField input_time_start;
    @FXML
    private Font x1;
    @FXML
    private TextField input_time_end;
    @FXML
    private TextField input_date;
    @FXML
    private ChoiceBox<Theater> choise_theater;
    @FXML
    private Button button_list;
    @FXML
    private Button button_search;
    @FXML
    private TextField input_movie;
    @FXML
    private ListView<Movie> list_output;
    

    @Override
    public void initialize(URL url, ResourceBundle rb) {

        kino = new Finnkino();
        
        updateTheaterList();
        
    }
    
    private void updateTheaterList() {
        choise_theater.getItems().clear();

        choise_theater.getItems().addAll(kino.getTheaters());
        
    }
    

    @FXML
    private void listMovies(ActionEvent event) {
        String inputDate = input_date.getText();
        
        String inputStartTime = input_time_start.getText();
        String inputEndTime = input_time_end.getText();
        
        Theater currentTheater = choise_theater.getValue();
        
        list_output.getItems().clear();
        if (currentTheater != null) {
            list_output.getItems().setAll(currentTheater.getMovieData(inputDate, inputStartTime, inputEndTime));
        
        
            input_time_start.setText(currentTheater.getStartTime());
            input_time_end.setText(currentTheater.getEndTime());
        }
    }

    @FXML
    private void searchByName(ActionEvent event) {
        String movieName = input_movie.getText();
        
        String inputDate = input_date.getText();
        
        String inputStartTime = input_time_start.getText();
        String inputEndTime = input_time_end.getText();
        
        System.out.println(movieName);
        
        list_output.getItems().clear();
        list_output.getItems().setAll(kino.findMoviesByName(movieName, inputDate, inputStartTime, inputEndTime));
        
    }

    
}
