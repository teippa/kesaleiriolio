/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kootutprojektit.h09;


import java.util.ArrayList;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.logging.Level;
import java.util.logging.Logger;


/**
 *
 * @author n1602
 */
public class Finnkino {
    
    //private String total;
    private final String kinoURL;
    
    private Document doc_locations;
    
    private ArrayList<Theater> theaters;
    
    private WebParser parser;
    
    
    public Finnkino() {
        kinoURL = "https://www.finnkino.fi/xml/TheatreAreas/";
        theaters = new ArrayList();
        
        parser = new WebParser();
        
        String sourceData;
        try {
            sourceData = parser.readSource(kinoURL);
            doc_locations = parser.parseXML(sourceData);
        } catch (Exception ex) {
            Logger.getLogger(Finnkino.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        getLocationData();
        
        
    }
    
    
    public ArrayList getTheaters() {
        return theaters;
    }
    
    
    private void getLocationData() {
        NodeList nodes = doc_locations.getElementsByTagName("TheatreArea");
        
        for(int i = 1; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            
            //System.out.println("\nCurrent Element :" + node.getNodeName());
            
            if (node.getNodeType() == Node.ELEMENT_NODE) {

                Element eElement = (Element) node;
                
                String newID = parser.getValue("ID", eElement);
                String newName = parser.getValue("Name", eElement);
                
                //System.out.println("ID: " + newID);
                //System.out.println("Nimi: " + newName);
                
                theaters.add(new Theater(newID, newName));
                
            } else {
                System.out.println("Node not found.");
            }
            
        }
    }
    
    
    public ArrayList findMoviesByName(String name, String inputDate, String inputStartTime, String inputEndTime) {
        ArrayList<Movie> foundMovies = new ArrayList();
        
        for (Theater t : theaters) {
            ArrayList<Movie> movies = t.getMovieData(inputDate, inputStartTime, inputEndTime);
            for (Movie m : movies) {
                if (m.getTitle().toLowerCase().contains(name.toLowerCase())) {
                    foundMovies.add(m);
                }
            }
        }
        
        
        return foundMovies;
    }
    
    
    
}
