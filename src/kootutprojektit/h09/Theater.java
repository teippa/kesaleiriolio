/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kootutprojektit.h09;

import java.text.ParseException;
import java.util.ArrayList;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author n1602
 */
public class Theater {
    
    private final WebParser parser;
    String ID, name;
    Document doc_movies;
    Date searchedDate = null;
    
    ArrayList<Movie> movies;
    
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
    SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
    
    private Date startTime;
    private Date endTime;
    
    
    public Theater() {
        ID = "0000";
        name = "Unknown";
        
        startTime = str2time("00:00");
        endTime = str2time("23:59");
        
        movies = new ArrayList();
        parser = new WebParser();
        
    }
    
    public Theater(String newID, String newName) {
        
        ID = newID;
        name = newName;
        
        startTime = str2time("00:00");
        endTime = str2time("23:59");
        
        movies = new ArrayList();
        parser = new WebParser();
        
    }
    
    public String getID() {
        return ID;
    }
    
    private Date getCurrentDate() {
        SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy");
        Date currentDate = null;//new Date();
        
        try {
            currentDate = dateFormat.parse(dateFormat.format(new Date()));
        } catch (ParseException ex) {
            Logger.getLogger(Theater.class.getName()).log(Level.SEVERE, null, ex);
        }
        return currentDate;
    }
    
    public String getURL() {
        String url;
        
        if (searchedDate == null) {
            url = "http://www.finnkino.fi/xml/Schedule/?area=" + ID;
        } else {
            url = "http://www.finnkino.fi/xml/Schedule/?area=" + ID + "&dt=" + dateFormat.format(searchedDate);
        }
        
        return url;
    }
    
    @Override
    public String toString() {
        return name;
    }
    
    public String getStartTime() {
        return timeFormat.format(startTime);
    }
    public String getEndTime() {
        return timeFormat.format(endTime);
    }
    
    
    private Date str2date(String str) {
        Date date = null;
        
        if (!str.trim().isEmpty()) {
            try {
                date = dateFormat.parse(str);
            } catch (ParseException ex) {
                Logger.getLogger(Theater.class.getName()).log(Level.SEVERE, null, ex);
            }

        }
        
        return date;
    }
    
        private Date str2time(String str){
        Date date = null;
        
        if (!str.trim().isEmpty()) {
            try {
                date = timeFormat.parse(str);
            } catch (ParseException ex) {
                Logger.getLogger(Theater.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
        
        return date;
    }
    
    private String setInputTimeFormatting(String str) {
        
        if (str.contains(":")) {
        } else if (str.contains(".")) {
            str = str.replace(".", ":");
        } else if (str.contains("-")) {
            str = str.replace(".", ":");
        } else {
            str = "00:00";
        }
        
        if (str.length() > 5) {
            str = "00:00";
        }
        
        return str;
        
        
    }
    
    
    
    
    public ArrayList getMovieData(String inputDate, String inputStartTime, String inputEndTime) {
        Date newDate = str2date(inputDate);
        
        // Muutetaan kellonajat date formaattiin
        inputStartTime = setInputTimeFormatting(inputStartTime);
        if ((inputEndTime = setInputTimeFormatting(inputEndTime)).equals("00:00")) {
            inputEndTime = "23:59";
        }
        startTime = str2time(inputStartTime);
        endTime = str2time(inputEndTime);
        
        // Päivämäärät eivät saa olla null-arvoja compareTo metodissa
        if (newDate == null) {
            newDate = getCurrentDate();
        }
        if (searchedDate == null) {
            searchedDate = newDate;
        }
        
        if (movies.isEmpty() | newDate.compareTo(searchedDate) != 0){
            searchedDate = newDate;
            
            movies.clear();
            movies = findMovies();
        }
        
        ArrayList<Movie> tempList = new ArrayList();
        
        
        for (Movie m : movies) {
            Date movieStart = str2time(m.getStartTimeStr());
            
            // Ei näytetä jo menneitä elokuvia
            if (m.getStartTime().compareTo(new Date()) > 0) {
                
                // aikarajojen tarkistus
                if ( movieStart.compareTo(startTime) > 0 && movieStart.compareTo(endTime) < 0 )
                    tempList.add(m);
            } // compareTo method returns the value greater than 0 if this Date is after the Date argument.
        }
        
        return tempList;
    }
    
    
    public ArrayList findMovies() {
        
        String url = getURL();
        doc_movies = parser.parseXML(parser.readSource(url));
        
        
        NodeList nodes = doc_movies.getElementsByTagName("Show");
        
        for(int i = 1; i < nodes.getLength(); i++) {
            Node node = nodes.item(i);
            
            //System.out.println("\nCurrent Element :" + node.getNodeName());
            
            if (node.getNodeType() == Node.ELEMENT_NODE) {

                Element eElement = (Element) node;
                
                String newID = parser.getValue("EventID", eElement);
                String title = parser.getValue("Title", eElement);
                String theater = parser.getValue("Theatre", eElement);
                String presentationMethod = parser.getValue("PresentationMethod", eElement);
                String lenght = parser.getValue("LengthInMinutes", eElement);
                String startTime = parser.getValue("dttmShowStart", eElement);
                String endTime = parser.getValue("dttmShowEnd", eElement);
                
                
                
                movies.add(new Movie(newID, title, theater, presentationMethod, lenght, startTime, endTime ));
                
            } else {
                System.out.println("Node not found.");
            }
            
            
        }
        return movies;
    }
    
    
}
