/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kootutprojektit.h09;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author n1602
 */
public class Movie {
    private final String eventID;
    private final String title;
    private final String theater;
    private final String presentationMethod;
    private final String length;
    
    
    private final Date startTime;
    private final Date endTime;
    
    
    
    
    public Movie(String eventID, String title, String theater, String presentationMethod, String length, String startTime, String endTime) {
        /*
        startTime = new Date();
        endTime = new Date();
        */
        this.eventID = eventID;
        this.title = title;
        this.theater = theater;
        this.presentationMethod = presentationMethod;
        this.length = length;
        
        
        this.startTime = str2time(startTime);
        this.endTime = str2time(endTime);
    
    }
    
    private Date str2time(String str) {
        Date time = null;
        SimpleDateFormat parseFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        
        str = str.replace("T", " ");
        
        try {
            time = parseFormat.parse(str);
        } catch (ParseException ex) {
            Logger.getLogger(Movie.class.getName()).log(Level.SEVERE, null, ex);
        }
        
        return time;
    }
    
    public String getTitle() {
        return title;
    }
    
    @Override
    public String toString() {
        String str = String.format("%1$s %2$s  ---  %3$s", dateTime2time(startTime), title, theater);
        //dateTime2time(startTime) + " " + theater + "\t" + title;
        return str;
    }
    
    public String dateTime2time(Date dateTime) {
        String time = "";
        SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm");
        
        return timeFormat.format(dateTime);
    }
    
    public Date getStartTime() {
        return startTime;
    }
    
    public String getStartTimeStr() {
        return dateTime2time(startTime);
    }
    
}

/*

<EventID>302588</EventID>
<Title>Jurassic World: Kaatunut valtakunta (2D)</Title>
<dttmShowStart>2018-06-09T14:00:00</dttmShowStart>
<dttmShowEnd>2018-06-09T16:18:00</dttmShowEnd>
<LengthInMinutes>128</LengthInMinutes>

<TheatreAuditorium>sali 1</TheatreAuditorium>
<PresentationMethod>2D</PresentationMethod>

*/