/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kootutprojektit.h09;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author n1602
 */
public class WebParser {
    
    
    
    
    public Document parseXML(String source) {
        
        Document doc = null;
        
        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            doc = dBuilder.parse(new InputSource(new StringReader(source)));
            doc.getDocumentElement().normalize();
        } catch (ParserConfigurationException e) {
            System.err.println("Caught ParserConfigurationException!");
        } catch (IOException e) {
            System.err.println("Caught IOException!");
        } catch (SAXException e) {
            System.err.println("Caught SAXException!");
        }
        
        return doc;
    }
    
    
    public String readSource(String urlStr) {
        
        String inputLine, gatheredData = "";
        
        try {
            URL url = new URL(urlStr);
        
            BufferedReader in = new BufferedReader(new InputStreamReader(url.openStream()));
            
            while((inputLine = in.readLine()) != null) {
                gatheredData += inputLine+"\n";
            }
        } catch (IOException ex) {
            Logger.getLogger(Finnkino.class.getName()).log(Level.SEVERE, null, ex);
        }/* catch (MalformedURLException ex) {
            Logger.getLogger(FinnkinoCrawler.class.getName()).log(Level.SEVERE, null, ex);
        }*/
        
        return gatheredData;
    }
    
    
    public String getValue(String tag, Element e) {
        return e.getElementsByTagName(tag).item(0).getTextContent();
    }
    
}
