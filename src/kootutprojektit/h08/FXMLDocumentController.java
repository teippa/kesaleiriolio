/**
 * Teijo Pekkola
 * It-Kesäleiri 2018
 * 1.6.2018
 */
package kootutprojektit.h08;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.RadioButton;
import javafx.scene.control.Slider;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.input.MouseEvent;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;

/**
 *
 * @author n1602
 */
public class FXMLDocumentController implements Initializable {
    
    
    BottleDispenser dispenser;
    private String selectedName;
    private double selectedSize = 1.5;
    private double selectedPrice;
    private Bottle selectedBottle;
    private Bottle lastBought = null;
    
    @FXML
    private Slider moneySlider;
    @FXML
    private Label label_moneyTotal;
    @FXML
    private Label label_inputAmount;
    @FXML
    private Button button_buy;
    @FXML
    private Label label_productPrice;
    @FXML
    private ComboBox<String> tuotteet;
    @FXML
    private TextField outputField;
    @FXML
    private Button button_kuitti;
    
    /**
     *
     * @param url
     * @param rb
     */
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        dispenser = BottleDispenser.getInstance();
        selectedPrice = -1;
        
        updateProductList();
        
        updateGUI();
        
    }
    
    /**
     * Triggeröidään kun päästetään lideristä irti. 
     * Päivittää laitteeseen laitettavan rahamäärän
     * @param event 
     */
    @FXML
    private void changeMoneyInput(MouseEvent event) {
        String output = String.format("%.2f", moneySlider.getValue());
        label_inputAmount.setText(output);
    }

    /**
     * "Lisää rahaa" -nappula. 
     * lisätään haluttu rahamäärä koneeseen
     * @param event 
     */
    @FXML
    private void addMoney(ActionEvent event) {
        dispenser.addMoney(moneySlider.getValue());
        //moneySlider.setValue(0);
        
        updateGUI();
    }

    /**
     * pudotusvalikko
     * Triggeröidään kun jokin tuote valitaan
     * @param event 
     */
    @FXML
    private void itemSelected(ActionEvent event) {
        String output;
        
        selectedName = tuotteet.getValue();
        updateGUI();
    }

    /**
     * "Osta" nappula
     * Ostetaan pullo
     * @param event 
     */
    @FXML
    private void buyBottle(ActionEvent event) {
        //dispenser.buyBottle(selectedItem);
        
        if ( selectedBottle != null ) {
            dispenser.buyBottle(selectedBottle);
            outputField.setText("KACHUNK! " + selectedBottle.getName() + " tipahti masiinasta!");
            
            button_kuitti.setDisable(false);
            lastBought = selectedBottle;
        }
        
        updateProductList();
        updateGUI();
    }

    /**
     * pullon koon valinta
     * @param event 
     */
    @FXML
    private void sizeSelected05(ActionEvent event) {
        selectedSize = 0.5;
        updateGUI();
    }
    @FXML
    private void sizeSelected15(ActionEvent event) {
        selectedSize = 1.5;
        updateGUI();
    }
    
    /**
     * Päivitetään pudotusvalikko
     */
    private void updateProductList() {
        ArrayList<Bottle> bottleArray = dispenser.getBottleArray();
        
        String previousBottle = tuotteet.getValue();
        
        if (bottleArray.isEmpty()) {
            tuotteet.setDisable(true);
        } else {
            
            ArrayList<String> availableNames = new ArrayList();

            for (Bottle btl : bottleArray) {
                if (!availableNames.contains(btl.getName()) ) {
                    availableNames.add(btl.getName());
                }
            }
            
            tuotteet.getItems().setAll(availableNames);
            
            if (availableNames.contains(previousBottle)) {
                tuotteet.setValue(previousBottle);
            } else {
                tuotteet.setValue(null);
            }
                
        }
    }
    
    /**
     * Yleispätevä päivitysfunktio
     */
    private void updateGUI() {
        updateMoneyTotal();
        updateProductDisplay();
        updatebuyButton();
        
    }
    
    /**
     * ostonappula on harmaa, jos ei voida ostaa
     */
    private void updatebuyButton() {
        
        if (0 < selectedPrice && selectedPrice <= dispenser.getMoney() && selectedBottle != null) {
            button_buy.setDisable(false);
        } else {
            button_buy.setDisable(true);
        }
    }
    
    /**
     * Päivitetään laitteessa olevat varat
     */
    private void updateMoneyTotal() {
        String output = String.format("%.2f", dispenser.getMoney());
        label_moneyTotal.setText(output);
    }
    
    /**
     * Päivitetään tuotteen hinta
     */
    private void updateProductDisplay() {
        selectedBottle = dispenser.findBottle(selectedName, selectedSize);
        
        String output;
        
        
        if (selectedBottle == null) {
            selectedPrice = -1;
            output = String.format("--.-- €");
        } else {
            selectedPrice = selectedBottle.getPrice();
            output = String.format("%.2f €", selectedPrice);
        }
        
        
        label_productPrice.setText(output);
    }
    
    /**
     * Poistetaan laitteesta rahat
     * @param event 
     */
    @FXML
    private void returnMoney(ActionEvent event) {
        String output;
        output = dispenser.returnMoney();
        
        outputField.setText(output);
        updateGUI();
    }
    
    /**
     * Tulostetaan kuitti kuitti.txt tiedostoon
     * @param event
     * @throws IOException 
     */
    @FXML
    private void tulostaKuitti(ActionEvent event) throws IOException {
        if (lastBought == null) {
            outputField.setText("Viimeisimmän ostoksen kuittia ei voida tulostaa.");
        } else {
            dispenser.tulostaKuitti(lastBought);
            outputField.setText("Kuitti tulostettu tiedostoon.");
        }
    }
    
}
