/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kootutprojektit.h08;

/**
 * @author n1602
 * Teijo Pekkola
 * IT-kesäleiri 2018
 * 29.5.2018
 */
public class Bottle {
    
    final private String name;
    final private String manufacturer;
    final private double total_energy;
    final private double size;
    final private double price;
    
    
    // Initialisointi, jos ei ole annettu parametrejä
    public Bottle() {
        name = "Pepsi Max";
        manufacturer = "Pepsi";
        total_energy = 0.3;
        size = 0.5;
        price = 1.80;
        
    }
    
    
    // Initialisointi custom arvoilla
    public Bottle(String bottleName, String manuf, double totE, double btlSize, double btlPrice) {
        name = bottleName;
        manufacturer = manuf;
        total_energy = totE;
        size = btlSize;
        price = btlPrice;
    }
    
    // palauttaa pullon nimen
    public String getName() {
        return name;
    }
    
    // palauttaa pullon valmistajan
    public String getManufacturer() {
        return manufacturer;
    }
    
    // palauttaa pullon energiasisällön
    public double getEnergy() {
        return total_energy;
    }
    
    
    // palauttaa pullon koon
    public double getSize() {
        return size;
    }

    // palauttaa pullon hinnan
    public double getPrice() {
        return price;
    }
    
}
