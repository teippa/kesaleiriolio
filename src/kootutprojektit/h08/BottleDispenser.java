/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kootutprojektit.h08;


import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
//import java.util.Scanner;

/**
 * @author n1602
 * Teijo Pekkola
 * IT-kesäleiri 2018
 * 29.5.2018
 */
public class BottleDispenser {
    
    private int bottles;
    private double money;

    // The array for the Bottle-objects
    private final ArrayList<Bottle> bottle_array;

    static private BottleDispenser bd = null;

    static public BottleDispenser getInstance() {
        if (bd == null) {
            bd = new BottleDispenser();
        }
        return bd;
    }
    
    
    /**
     * Luodaan lista pulloista jotka ovat koneessa ja alustetaan muuttujat 
    */
    private BottleDispenser() {

        bottles = 6;
        money = 0;

        // Initialize the array1

        bottle_array = new ArrayList();

        // Add Bottle-objects to the array
        
        bottle_array.add(new Bottle());
        bottle_array.add(new Bottle("Pepsi Max", "Pepsi", 0.3, 1.5, 2.2));
        bottle_array.add(new Bottle("Coca-Cola Zero", "Coca-Cola", 0.3, 0.5, 2.0));
        bottle_array.add(new Bottle("Coca-Cola Zero", "Coca-Cola", 0.3, 1.5, 2.5));
        bottle_array.add(new Bottle("Fanta Zero", "Coca-Cola", 0.3, 0.5, 1.95));
        bottle_array.add(new Bottle("Fanta Zero", "Coca-Cola", 0.3, 0.5, 1.95));

        /*
        for(int i = 0;i<bottles;i++) {
            
            // Use the default constructor to create new Bottles
            bottle_array.add(new Bottle());

        }
        */

    }



    /**
     * Lisätään rahaa koneeseen
    */
    public void addMoney(double amount) {
        money += amount;
    }
    
    public double getMoney() {
        return money;
    }

    public ArrayList getBottleArray() {
        return bottle_array;
    }
    


    /**
     * Ostetaan pullo. Käytetään rahaa ja poistetaan pullo koneesta 
    */
    public void buyBottle(Bottle selectedBottle) {
        
        if (bottle_array.contains(selectedBottle)) {
            
            double bottlePrice = selectedBottle.getPrice();


            if ( money >= bottlePrice ) {
                //System.out.println("KACHUNK! " + selectedBottle.getName() + " tipahti masiinasta!");
                bottle_array.remove(selectedBottle);
                bottles -= 1;
                money -= bottlePrice;
            } else {
                //System.out.println("Syötä rahaa ensin!");
            } 
        } else {
            //System.out.println("Kyseistä pulloa ei ole!");
        }
    }
    
    /**
     * Etsitään nimeä ja kokoa vastaava pullo koneesta.
     * Palautetaan null, jos ei löytynyt
     * @param selectedName
     * @param selectedSize
     * @return 
     */
    public Bottle findBottle(String selectedName, double selectedSize) {
        Bottle foundBottle = null;
        
        
        for (Bottle btl : bottle_array) {
            if (btl.getName().equals(selectedName) && selectedSize == btl.getSize() ) { 
                foundBottle = btl;
                break;
            }
        }
        
        return foundBottle;
    }

    /**
     * Poistetaan kaikki rahat koneesta 
    */
    public String returnMoney() {
        
        //String message = String.format("Klink klink. Sinne menivät rahat! Rahaa tuli ulos %.2f€", money);

        //System.out.println(message);
        
        String output;
        
        if (money > 0) {
            output = String.format("Klink klink. Sinne menivät rahat! Rahaa tuli ulos %.2f€", money);
        } else {
            output = "Koneessa ei ole rahaa.";
        }
        
        money = 0;
        return output;
    }
    
    /**
     * Tulostetaan koitti viimeisimmästä ostoksesta tiedostoon
     * @param lastBought
     * @throws IOException 
     */
    public void tulostaKuitti(Bottle lastBought) throws IOException {
        try (BufferedWriter out = new BufferedWriter(new FileWriter("kuitti.txt"))) {
            out.write("Kuitti" + System.lineSeparator()
                    + lastBought.getName() + " (" + lastBought.getSize() + " l) . . . . . . . . . . . . " + lastBought.getPrice() + " €" + System.lineSeparator()
                    + System.lineSeparator());
            out.write("Kiitos käynnistä!");
            out.close();
        }
        
    }
    
}
