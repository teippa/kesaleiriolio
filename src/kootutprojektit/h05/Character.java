/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kootutprojektit.h05;

import java.util.Scanner;

/**
 *
 * @author n1602
 */
public class Character {
    protected String name;
    
    protected WeaponBehavior weapon;
    
    public Character() {
        name = this.getClass().getSimpleName();
        
        
        weapon = getWeapon();
        
    }
    
    public void fight() {
        if (weapon != null) {
            System.out.println(name + " tappelee aseella " + weapon.useWeapon());
        } else {
            System.out.println(name + " tappelee paljain käsin");
        }
    }
    
    private WeaponBehavior getWeapon() {
        Scanner scan = new Scanner(System.in);
        String valinta;
        WeaponBehavior newWeapon;
        
        System.out.println("Valitse aseesi: ");
        System.out.println("1) Veitsi");
        System.out.println("2) Kirves");
        System.out.println("3) Miekka");
        System.out.println("4) Nuija");
        System.out.print("Valintasi: ");
        valinta = scan.nextLine();
        
        switch (valinta) {
            case "1":
                newWeapon = new KnifeBehavior();
                break;
            case "2":
                newWeapon = new AxeBehavior();
                break;
            case "3":
                newWeapon = new SwordBehavior();
                break;
            case "4":
                newWeapon = new ClubBehavior();
                break;
            default:
                newWeapon = null;
                System.out.println("Tuntematon valinta.");
                break;
        }
        return newWeapon;
    }
}


class King extends Character {
}

class Queen extends Character {
}

class Knight extends Character {
}

class Troll extends Character {
}


/****************************************************/


abstract class WeaponBehavior {
    String name;
    
    public WeaponBehavior() {
        name = "Kädet";
    }
    
    public String useWeapon() {
        return name;
    }
    
}


class ClubBehavior extends WeaponBehavior {
    
    public ClubBehavior() {
        name = "Club";
    }
}

class SwordBehavior extends WeaponBehavior {
    
    public SwordBehavior() {
        name = "Sword";
    }
}

class AxeBehavior extends WeaponBehavior {
    
    public AxeBehavior() {
        name = "Axe";
    }
}

class KnifeBehavior extends WeaponBehavior {
    
    public KnifeBehavior() {
        name = "Knife";
    }
}

