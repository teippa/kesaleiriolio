/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kootutprojektit.h05;

import java.util.Scanner;

/**
 * @author n1602
 * Teijo Pekkola
 * IT-Kesäleiri 2018
 * 30.5.2018
 */
public class H5 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        
        System.out.println("\n*-*-*-*-*-*-* T4 *-*-*-*-*-*-*\n");
        
        Car auto = new Car();
        
        auto.print();
        
        
        System.out.println("\n*-*-*-*-*-*-* T5 *-*-*-*-*-*-*\n");
        
        // TODO code application logic here
        
        boolean run = true;
        String valinta;
        Character hahmo = null;
        
        Scanner scan = new Scanner(System.in);
        
        while (run) {
            System.out.println("*** TAISTELUSIMULAATTORI ***");
            System.out.println("1) Luo hahmo");
            System.out.println("2) Taistele hahmolla");
            System.out.println("0) Lopeta");
            System.out.print("Valintasi: ");
            valinta = scan.nextLine();
        
            switch (valinta) {
                case "1":
                    hahmo = characterSelection();
                    break;
                case "2":
                    if (hahmo == null) {
                        System.out.println("Luo hahmo ensin.");
                    } else {
                        hahmo.fight();
                    }
                    break;
                case "0":
                    run = false;
                    break;
                default:
                    System.out.println("Tuntematon valinta");
                    break;
            }
            
            
        }
        
    }
    
    private static Character characterSelection() {
        String valinta;
        Character ukko;
        Scanner scan = new Scanner(System.in);
        
        System.out.println("Valitse hahmosi: ");
        System.out.println("1) Kuningas");
        System.out.println("2) Ritari");
        System.out.println("3) Kuningatar");
        System.out.println("4) Peikko");
        System.out.print("Valintasi: ");
        
        valinta = scan.nextLine();
        
        switch (valinta) {
            case "1":
                ukko = new King();
                break;
            case "2":
                ukko = new Knight();
                break;
            case "3":
                ukko = new Queen();
                break;
            case "4":
                ukko = new Troll();
                break;
            default:
                ukko = null;
        }
        
        return ukko;
    }
    
    
}