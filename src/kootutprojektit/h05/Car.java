/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kootutprojektit.h05;



/**
 * @author n1602
 * Teijo Pekkola
 * IT-Kesäleiri 2018
 * 30.5.2018
 */
import java.util.ArrayList;
public class Car {
    
    int wheelCount;
    
    ArrayList<Wheel> wheels;
    Body body;
    Chassis chassis;
    Engine engine;
    
    
    
    public Car() {
        wheelCount = 4;
        
        body = new Body();
        chassis = new Chassis();
        engine = new Engine();
        
        wheels = new ArrayList<Wheel>(wheelCount);
        for (int i=0 ; i<wheelCount ; i++) {
            wheels.add(new Wheel());
        }
        
    }
    
    public void print() {
        System.out.println("Autoon kuuluu:");
        System.out.println("\t" + body.getName());
        System.out.println("\t" + chassis.getName());
        System.out.println("\t" + engine.getName());
        System.out.println("\t" + wheelCount + " " +  wheels.get(0).getName());
    }
    
}

abstract class Part {
    
    String name;
    
    Part() {
    }
    
    public String getName() {
        return name;
    }

}

class Body extends Part {
    
    Body() {
        name = this.getClass().getSimpleName();
        System.out.println("Valmistetaan: " + name);
    }
    
}

class Wheel extends Part {
    
    Wheel() {
        name = this.getClass().getSimpleName();
        System.out.println("Valmistetaan: " + name);
        
    }
}

class Chassis extends Part {
    
    Chassis() {
        name = "Chassis";
        System.out.println("Valmistetaan: " + name);
    }
}

class Engine extends Part {
    
    Engine() {
        name = "Engine";
        System.out.println("Valmistetaan: " + name);
    }
}