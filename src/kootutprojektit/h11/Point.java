/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kootutprojektit.h11;


import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Circle;
import javafx.scene.paint.Color;

/**
 *
 * @author n1602
 */
public class Point {
    Circle c;
    
    private final double pos_x;
    private final double pos_y;
    private final double size = 5;
    private final Color fillColor = Color.RED;
    private final Color strokeColor = Color.BLACK;
    
    private boolean alive = true;
    private boolean clicked = false;
            
    public Point(double pos_x, double pos_y) {
        this.pos_x = pos_x;
        this.pos_y = pos_y;
        
        c = new Circle(pos_x, pos_y, size, fillColor);
        
        c.setStroke(strokeColor);
        
        c.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                //System.out.println("mouse click detected! "+event.getSource() + event.getButton().name());
                
                if (event.getButton().name().equals("PRIMARY")) {
                    System.out.println("Hei, olen piste");
                    c.setFill(Color.YELLOW);
                    clicked = true;
                } else if (event.getButton().name().equals("SECONDARY")) {
                    if (clicked) {
                        clicked = false;
                        c.setFill(fillColor);
                    } else {
                        c.setFill(Color.AQUA);
                        alive = false;
                    }
                }
            }
        });

        
    }
    
    public double getDistance(double pos_x, double pos_y) {
        double distSquared = ((pos_x - this.pos_x)*(pos_x - this.pos_x) + ((pos_y - this.pos_y)*(pos_y - this.pos_y)));
        return distSquared;
    }
    
    public Circle get() {
        return c;
    }
    
    public double getSize() {
        return size;
    }
    
    public boolean isClicked() {
        return clicked;
    }
    
    public void setClicked(boolean clicked) {
        this.clicked = clicked;
    }
    
    public boolean isAlive() {
        return alive;
    }
    
    public double getX() {
        return pos_x;
    }
    
    public double getY() {
        return pos_y;
    }
    
    public void setColor(Color color) {
        c.setFill(color);
    }
    
}
