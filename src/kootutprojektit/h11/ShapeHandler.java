/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kootutprojektit.h11;

import java.util.ArrayList;
import javafx.scene.paint.Color;

/**
 *
 * @author n1602
 */
public class ShapeHandler {
    static private ShapeHandler sh = null;
    
    private final ArrayList<Point> points;
    private final ArrayList<Path> paths;
    //private final ArrayList<Line> lines;
    
    
    public static ShapeHandler getInstance() {
        if (sh == null) {
            sh = new ShapeHandler();
        }
        return sh;
    }
    
    private ShapeHandler() {
        points = new ArrayList();
        paths = new ArrayList();
    }
    
    public Point createPoint(double pos_x, double pos_y) {
        Point p = new Point(pos_x, pos_y);
        points.add(p);
        return p;
    }
    
    public void clearPoints() {
        points.clear();
    }
    
    public void clearPaths() {
        paths.clear();
    }
    
    public ArrayList getPoints() {
        return points;
    }
    
    public ArrayList getPaths() {
        return paths;
    }
    
    public void addPath(Path p) {
        paths.add(p);
    }
    
    public boolean removePoint(Point p) {
        return points.remove(p);
    }
    
    public boolean removePath(Path p) {
        return paths.remove(p);
    }
    
    public Path tryToCreatePath() {
        Point p1 = null;
        Point p2 = null;
        Path path = null;
        
        for (Point p : points) {
            if (p.isClicked() && p1 == null) {
                p1 = p;
            } else if (p.isClicked() && p2 == null) {
                p2 = p;
            }
            if (p1 != null && p2 != null) {
                path = createPath(p1, p2);
                break;
            }
        }
        return path;
    }
    
    private Path createPath(Point p1, Point p2) {
        p1.setClicked(false);
        p1.setColor(Color.RED);
        p2.setClicked(false);
        p2.setColor(Color.RED);
        Path path = new Path(p1, p2);
        return path;
    }
    
}
