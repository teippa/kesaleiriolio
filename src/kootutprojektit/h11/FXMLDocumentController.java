/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kootutprojektit.h11;

import java.net.URL;
import java.util.ArrayList;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.ToggleButton;
import javafx.scene.input.MouseButton;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Pane;

/**
 *
 * @author n1602
 */
public class FXMLDocumentController implements Initializable {
    
    ShapeHandler sh = ShapeHandler.getInstance();

    @FXML
    private AnchorPane backround;
    @FXML
    private ToggleButton lineToggler;
    
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
    }    

    @FXML
    private void mouseDown(MouseEvent event) {
        
        double pos_x = event.getSceneX();
        double pos_y = event.getSceneY();
        
        MouseButton btn = event.getButton();
        
        //System.out.println("klik\n" + pos_x + "\t" + pos_y + "\t" + btn.name());
        switch (btn.name()) {
            case "PRIMARY":
                if (!detectCircles(pos_x, pos_y)) {
                    createCircle(pos_x, pos_y);
                }
                break;
            case "SECONDARY":
                removeDeadCircles();
                removeDeadPaths();
                break;
            case "MIDDLE":
                break;
            default:
                break;
        }
        
        
    }
    
    private void removeDeadPaths() {
        ArrayList<Path> tempPaths = new ArrayList();
        tempPaths.addAll(sh.getPaths());

        if (!tempPaths.isEmpty()) {
            //System.out.println("Poistetaan kuolleita viivoja");
            for (Path p : tempPaths) {
                if (p.isAlive() == false) {
                    backround.getChildren().remove(p.get());
                    sh.removePath(p);
                }
            }
        }
    }
    
    
    private void removePaths() {
        ArrayList<Path> tempPaths = new ArrayList();
        tempPaths.addAll(sh.getPaths());

        if (!lineToggler.isSelected() && !tempPaths.isEmpty()) {
            //System.out.println("Poistetaan viivoja");


            for (Path p : tempPaths) {
                backround.getChildren().remove(p.get());
                sh.removePath(p);
            }
        }
    }
    

    @FXML
    private void mouseUp(MouseEvent event) {

        Path newPath = sh.tryToCreatePath();
        if ((newPath) != null) {
            removePaths();
            
            sh.addPath(newPath);
            
            backround.getChildren().add(newPath.get());
            backround.getChildren().get(backround.getChildren().size()-1).toBack();
        }
    }
    
    private void createCircle(double pos_x, double pos_y) {
        Point p = sh.createPoint(pos_x, pos_y);
        
        backround.getChildren().add(p.get());
    }
    
    private void removeDeadCircles() {
        //sh.clearPoints();
        //backround.getChildren().clear();
        
        ArrayList<Point> tempPoints = new ArrayList();
        tempPoints.addAll(sh.getPoints());
        
        for (Point p : tempPoints) {
            if (p.isAlive() == false) {
                backround.getChildren().remove(p.get());
                sh.removePoint(p);
            }
        }
    }
    
    private boolean detectCircles(double pos_x, double pos_y) {
        boolean found = false;
        ArrayList<Point> tempPoints = new ArrayList();
        tempPoints.addAll(sh.getPoints());
        for (Point p : tempPoints) {
            //System.out.println(p.getDistance(pos_x, pos_y));
            if (p.getDistance(pos_x, pos_y) < 36) {
                found = true;
            } 
        }
        return found;
    }

    @FXML
    private void testi(ActionEvent event) {
        removePaths();
    }
    
    
}
