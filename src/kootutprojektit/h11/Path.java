/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kootutprojektit.h11;

import javafx.event.EventHandler;
import javafx.scene.input.MouseEvent;
import javafx.scene.shape.Line;
import javafx.scene.paint.Color;

/**
 *
 * @author n1602
 */
public class Path {
    Line l;
    
    private double length = 0;
    
    Point parentPoint1;
    Point parentPoint2;
    
    public Path(Point p1, Point p2) {
        l = new Line();
        
        l.setStartX(p1.getX());
        l.setStartY(p1.getY());
        l.setEndX(p2.getX());
        l.setEndY(p2.getY());
        
        parentPoint1 = p1;
        parentPoint2 = p2;
        
        l.setFill(Color.RED);
        
        length = Math.sqrt(Math.pow((p1.getX() - p2.getX()), 2) + Math.pow((p1.getY() - p2.getY()), 2));
        
        
        l.setOnMousePressed(new EventHandler<MouseEvent>() {
            @Override
            public void handle(MouseEvent event) {
                //System.out.println("mouse click detected! "+event.getSource() + event.getButton().name());
                
                if (event.getButton().name().equals("PRIMARY")) {
                    System.out.println(String.format("Viivan pituus: %.2fkm", getDistance()));
                }
            }
        });
    }
    
    public Line get() {
        return l;
    }
    
    
    private double getDistance() {
        double correctionValue = 1.19936;
        return length * correctionValue;
    }

    
    public boolean isAlive() {
        boolean alive = true;
        if (parentPoint1 == null || parentPoint1.isAlive() == false) {
            alive = false;
        } else if (parentPoint2 == null || parentPoint2.isAlive() == false) {
            alive = false;
        }
        return alive;
    }
    
    
    
}
