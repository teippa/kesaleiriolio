/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package kootutprojektit.h10;

import java.net.URL;
import java.util.ListIterator;
import java.util.ResourceBundle;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebHistory.Entry;
import javafx.scene.web.WebView;

/**
 *
 * @author n1602
 */
public class FXMLDocumentController implements Initializable {
    
    ObservableList<Entry> history;
    ListIterator<Entry> iter;
    WebEngine engine;

    @FXML
    private WebView web_window;
    @FXML
    private Button button_previous;
    @FXML
    private Button button_next;
    @FXML
    private TextField input_URL;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        engine = web_window.getEngine();
        engine.load("https://www.google.com/");
        
    }   
    
    private void iteratorGoesLast() {
        while(iter.hasNext()) {
            System.out.println(history.get(iter.nextIndex()).getUrl());
            iter.next();
        }
        System.out.println("");
    }

    @FXML
    private void previousPage(ActionEvent event) {
        engine.executeScript("window.history.back();");
    }

    @FXML
    private void nextPage(ActionEvent event) {
        engine.executeScript("window.history.forward();");
    }

    @FXML
    private void search(ActionEvent event) {
        String input = input_URL.getText();
        
        
        if (input.equals("index.html")) {
            engine.load(getClass().getResource("index.html").toExternalForm());
            
        } else if (input.contains("http://")) {
            engine.load(input);
        } else if (input.trim().isEmpty()) {
        } else {
            input = "http://" + input;
            engine.load(input);
        }
        //input_URL.clear();
    }

    @FXML
    private void refresh(ActionEvent event) {
        engine.reload();
        //iteratorGoesLast();
    }

    @FXML
    private void javascript_shoutOut(ActionEvent event) {
        engine.executeScript("document.shoutOut();");
    }

    @FXML
    private void javascript_initialize(ActionEvent event) {
        engine.executeScript("initialize();");
    }
    
}
